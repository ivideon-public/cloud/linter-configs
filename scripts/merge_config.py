import configparser

import urllib3


class ConfigParser:
    """
    ConfigParser initial class.

    :param local_cfg: Path to local setup.cfg.
    :param remote_cfg: Remote setup.cfg url.
    :type local_cfg: str
    :type remote_cfg: str
    """

    def __init__(self, local_cfg, remote_cfg):
        self.local_cfg = f'{local_cfg}/setup.cfg'
        self.remote_cfg = remote_cfg

    def get_remote_config(self, url):
        """
        Return data from url.

        :param url: Parse url.
        :type url: str
        :return: Parsed url.
        """
        http = urllib3.PoolManager()
        response = http.request('GET', url)
        return response.data.decode('utf-8')

    def merge_configs(self):
        """Merge remote to local setup.cfg configuration."""
        config = configparser.ConfigParser()
        if self.remote_cfg:
            config.read_string(self.get_remote_config(url=self.remote_cfg))
        else:
            config.read_file(open('/config/setup.cfg'))

        parser = configparser.ConfigParser()
        try:
            parser.read_file(open(self.local_cfg))
        except FileNotFoundError:
            print('No such file setup.cfg')

        for sector in parser.sections():
            if sector in config.sections():
                for key in parser[sector]:
                    value_result = parser.get(sector, key, raw=False)
                    config.set(sector, key, value_result)
            else:
                config[sector] = {}
                for key in parser[sector]:
                    value_result = parser.get(sector, key, raw=False)
                    config.set(sector, key, value_result)

        with open('/var/merged_config.cfg', 'w') as configfile:
            config.write(configfile)
