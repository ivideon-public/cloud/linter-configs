import os
import subprocess
import sys
import json
from glob import glob
from shutil import copyfile

import requests

from merge_config import ConfigParser

LOCAL_CFG_PATH = '/src'
REMOTE_CFG_URL = ''
SAFETY_DB_URL = 'https://raw.githubusercontent.com/pyupio/safety-db/master/data'
ORIGINAL_SETUP_FILE = '/src/setup.cfg'
TMP_SETUP_FILE = '/tmp/original_setup.cfg'
MERGE_SETUP_FILE = '/var/merged_config.cfg'


def merge_configs():
    """Выполняет мердж локального и глобального файла setup.cfg."""
    cfg = ConfigParser(
        local_cfg=os.getenv('LOCAL_CFG_PATH', default=LOCAL_CFG_PATH),
        remote_cfg=os.getenv('REMOTE_CFG_URL', default=REMOTE_CFG_URL),
    )
    cfg.merge_configs()


def scan_requirements():
    """
    Ищет все пути к requirements.txt в проекте.

    :return: Список всех путей к requirements.txt,
    например: [ '/src/requirements.txt' , '/src/foo/requirements.txt' ]   
    """
    requirements_files = [
        req_file_path for req_file_path in glob('/src/' + "**/*.txt", recursive=True)
        if 'requirements.txt' in req_file_path and not 'env' in req_file_path
    ]
    return requirements_files


def executor(command, stdout=None):
    """
    Выполняет команду оболочки.

    :return: объект процесса.
    """
    process = subprocess.Popen(command, cwd='/src/', stdout=stdout)
    process.wait()
    return process


def run_flake(flake_arg):
    """
    Выполняет запуск flake8.

    :return: код завершения процесса flake8.
    """
    proc = executor(['flake8', flake_arg])
    return proc.returncode


def run_safety():
    """
    Выполняет запуск safety.

    :return: код завершения процесса safety.
    """
    for db_file in ['insecure.json', 'insecure_full.json']:
        response = requests.get(f'{SAFETY_DB_URL}/{db_file}')
        with open(f'/tmp/safety-db/{db_file}', 'wb') as db_insecure_file:
            db_insecure_file.write(response.content)

    requirements_paths = scan_requirements()

    exit_codes = []
    insecure_report = []
    for req_path in requirements_paths:
        proc = executor(
            ['safety', 'check', '--ignore=39462', '--full-report', '--json', '--db=/tmp/safety-db', '-r', req_path],
            stdout=subprocess.PIPE,
        )

        exit_codes.append(proc.returncode)

        stdout, _ = proc.communicate()
        safety_report = json.loads(stdout)

        if safety_report:
            insecure_report.append(f'From: {req_path}\n' + '-' * 100)
            for report in safety_report:
                insecure_report.append(
                    'Package: {0}\n Installed: {1}\n Affected: {2}\n Reason: {3}\n ID: {4}\n'.format(*report)
                )

    if insecure_report:
        pale = '#' * 30
        print(pale + ' Security Report ' + pale + '\n')
        for report in insecure_report:
            print(report)

    return set(exit_codes)


def run_analyzer(args):
    """Запускает поочерёдно flake8 и safety."""
    exit_code_list = []

    if 'no-flake' not in args:
        flake_code = run_flake(args[0])
        exit_code_list.append(flake_code)

    if 'no-safety' not in args:
        safety_codes = run_safety()
        exit_code_list += safety_codes

    linter_code = sum(set(exit_code_list))

    if linter_code != 0:
        sys.exit(1)
    else:
        sys.exit(0)


def main(args):
    if args[0] == 'd':
        subprocess.run(['cat', MERGE_SETUP_FILE])
        sys.exit(0)

    if os.path.isfile(ORIGINAL_SETUP_FILE):
        try:
            copyfile(ORIGINAL_SETUP_FILE, TMP_SETUP_FILE)
            copyfile(MERGE_SETUP_FILE, ORIGINAL_SETUP_FILE)
            run_analyzer(args)
        except Exception as err:
            print(err)
        finally:
            copyfile(TMP_SETUP_FILE, ORIGINAL_SETUP_FILE)
    else:
        try:
            copyfile(MERGE_SETUP_FILE, ORIGINAL_SETUP_FILE)
            run_analyzer(args)
        except Exception as err:
            print(err)
        finally:
            os.remove(ORIGINAL_SETUP_FILE)


if __name__ == '__main__':
    args = sys.argv[1:]
    merge_configs()
    main(args)
