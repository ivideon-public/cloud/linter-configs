install:
	python3 setup.py install

build:
	python3 setup.py sdist

publish:
	python3 setup.py sdist upload -r ivideon-private

build_image:
	docker build -t registry.gitlab.com/ivideon-public/cloud/linter-configs:latest .

publish_image:
	docker push registry.gitlab.com/ivideon-public/cloud/linter-configs:latest
