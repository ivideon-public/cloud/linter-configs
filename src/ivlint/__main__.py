import argparse
import os

import docker
from docker.errors import APIError


IMAGE = 'registry.gitlab.com/ivideon-public/cloud/linter-configs:latest'
ROOT_DIR = os.path.abspath(os.getcwd())


class Container:
    """
    Класс описывает запуск docker контейнера со статическим анализатором flake8.
    """

    def __init__(self):
        self.client = docker.from_env()
        self.container = None

    def pull_image(self):
        """
        Выполняет pull образа.
        """
        self.client.images.pull(IMAGE)

    def run_container(self, linter_args, project_path=ROOT_DIR):
        """
        Выполняет запуск контейнера со статическим анализатором.

        :param linter_args: необязательные аргументы для скрипта запуска анализатора start.sh.
        :param project_path: необязательный аргумент для указания пути к проверяемому проекту.
        """
        self.container = self.client.containers.run(
            image=IMAGE,
            environment=[f'LINTER_ARGS={linter_args}'],
            volumes={project_path: {'bind': '/src', 'mode': 'rw'}},
            detach=True,
        )

    def kill_container(self):
        """
        Останавливает процесс запущенного контейнера.
        """
        self.container.kill()

    def remove_container(self):
        """
        Удаляет остановленный контейнер.
        """
        self.container.remove()

    def print_logs(self):
        """
        Печатает результат stdout контейнера.
        """
        for line in self.container.logs(stream=True):
            print(line.decode().strip())


def run_lint(args, linter):
    """
    Создаёт экземпляр класса Container и выполняет запуск анализатора в зависимости от
    переданных аргументов в argeparse.

    :param args: набор аргументов, передаваемых при запуске анализатора.
    :param linter: инииализированный экземпляр класса контейнера с линтером.
    """
    if args.noupdate:
        print('ivlint update \33[32mskipped\033[0m')
    else:
        try:
            print('ivlint update \33[32mstarted\033[0m')
            linter.pull_image()
            print('ivlint update \33[32mcomplete\033[0m\n')
        except APIError:
            print('ivlint update is not possible. Running linter without update.')

    print('ivlint \33[32mstarted\033[0m\n')

    if args.debug and args.path:
        linter.run_container(linter_args='d', project_path=args.path)
    elif args.debug:
        linter.run_container(linter_args='d')
    elif args.file:
        linter.run_container(linter_args=args.file)
    elif args.path:
        linter.run_container(project_path=args.path)
    else:
        linter.run_container()

    linter.print_logs()
    print('\nivlint \33[32mcomplete\033[0m')


def check_path(path):
    """
    Проверяет, является ли путь каталогом.

    :param path: проверяемый путь.
    :raises NotADirectoryError: проверяемый путь не является директорией.
    """
    if os.path.isdir(path):
        return path
    raise NotADirectoryError(path)


def main():
    """
    Выполняет обработку переданных в скрипт аргументов и выполняет запуск
    статического анализатора.
    """
    parser = argparse.ArgumentParser(
        add_help=True,
        prefix_chars='--/',
        description='',
    )

    parser.add_argument(
        '-n',
        '--no-update',
        action='store_true',
        help='disable update ivlint',
        dest='noupdate',
    )
    parser.add_argument(
        '-d',
        '--debug',
        action='store_true',
        help='show merged setup.cfg and exit',
        dest='debug',
    )
    parser.add_argument(
        '-p',
        '--path',
        action='store',
        help='path to project',
        dest='path',
        type=check_path,
    )
    parser.add_argument(
        'file',
        help='path to file',
        nargs='?',
        default='.',
    )

    args = parser.parse_args()
    linter = Container()

    try:
        run_lint(args, linter)
        linter.remove_container()
    except KeyboardInterrupt:
        linter.kill_container()
        linter.remove_container()


if __name__ == '__main__':
    main()
