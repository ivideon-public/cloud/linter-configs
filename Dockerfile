FROM python:3.9-alpine

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt && rm /tmp/requirements.txt

RUN mkdir /src /scripts /config /tmp/safety-db
COPY scripts/* /scripts/
COPY styles/setup.cfg /config

ARG ARGS
ARG REMOTE_CFG_URL_ARG
ENV LINTER_ARGS=${ARGS}
ENV REMOTE_CFG_URL=${REMOTE_CFG_URL_ARG}

ENTRYPOINT python3 /scripts/start.py ${LINTER_ARGS}
