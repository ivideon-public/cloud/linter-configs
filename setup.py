from setuptools import setup, find_packages


setup(
    name='ivlint',
    version='1.2.4',
    description=('Утилила для удобного запуска linter-configs при локальном использовании.'),
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=['docker'],
    entry_points={
        'console_scripts': [
            'ivlint = ivlint.__main__:main',
        ],
    },
)
