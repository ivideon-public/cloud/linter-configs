# Flake8

## Страница проекта
https://flake8.pycqa.org/en/latest/

Мы используем модифицированную версию от компании `wemake services`

https://wemake-python-stylegui.de/en/latest/

## Установка и настройка

```bash
pip install wemake-python-styleguide
```

В репозиторий должен быть добавлен файл [setup.cfg](../setup.cfg)


## Запуск

```bash
flake8 <filename>
```

## Настройка для Pycharm

1. Заходим в `Settings->Tools->File Watchers`
2. настриваем следующие параметры:
    * File type: Python
    * Scope: Current File
    * Program: `$PyInterpreterDirectory$/flake8` (чтобы использовать
    установленный venv)
    * Arguments: `$FileDir$/$FileName$`
    * Show console: Never
    * Output filters: `$FILE_PATH$:$LINE$:$COLUMN$: $MESSAGE$` (для pycharm)

Пример:

![настройки pycharm](pycharm1.png)


## Настройка ivlint для Pycharm

1. Нужный репозиторий помечается как sources root.

![](as_root.png)

2. В Settings -> Tools -> File Watchers создается новый File Watcher:
    * FileType: Python
    * Scope: Current File
    * Program: ivlint
    * Arguments: -n $FilePathRelativeToSourcepath$ (если скорость непринципиальна, то можно и без -n)
    * Working directory: $SourcepathEntry$
    * Show console: On error
    * Output filters: $LINE$:$COLUMN$ $MESSAGE$

![](fw.png)

3. В Settings -> Editor -> Inspections включается File Watchers -> File watcher problems

![](fwp.png)

Результат:

![](result.gif)
