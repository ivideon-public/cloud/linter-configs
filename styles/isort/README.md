# Isort

## Страница проекта

https://github.com/timothycrosley/isort

## Установка

```bash
pip install isort
```

## Использование

Для работы с isort потребуется файл конфига `setup.cfg` с заполненным сектором isort, сам файл [тут](../setup.cfg). В нем следует
подправить строку `known_this_repo`

Затем перед коммитом вызываем команду в каталоге репозитория.

```bash
isort
```
