Linter configs
==============
linter-configs - сервис для запуска статического анализатора [wemake-python-styleguide](https://github.com/wemake-services/wemake-python-styleguide) через docker.
ivlint - утилита для командной строки, выполняющая запуск linter-configs.

Требования
----------
Для запуска linter-configs потребуется установленный [docker](https://docs.docker.com/engine/install/ubuntu/) последней версии, а также python3.6 и выше.

Быстрый старт
-------------
linter-configs является инструментом для командной строки, который выполняет проверку кода на соответствие требованиям, установленным в [CodeStyle](https://ivideon.atlassian.net/wiki/spaces/API/pages/1316978753/Python+Style+Guide+V2.0).

Вариант 1:

Установить ivlint:
```bash
pip3 install ivlint
```

Авторизоваться в registry.gitlab.com:

```bash
docker login registry.gitlab.com
```

Выполнить в корне проекта:
```bash
ivlint
```

Вариант 2:

Авторизоваться в registry.gitlab.com:

```bash
docker login registry.gitlab.com
```

Скачать последнюю версию linter-configs:

```bash
docker pull registry.gitlab.com/ivideon-public/cloud/linter-configs:latest
```

Выполнить в корне проекта:
```bash
docker run --rm -v "$PWD:/src" registry.gitlab.com/ivideon-public/cloud/linter-configs:latest
```

Аргументы принимаемые ivlint
----------------------------

| key | default | description |
|-----|---------|-------------|
| `-h`, `--help` | False | Отобразить `help`. |
| `-n`, `--no-update` | True | Отключить обновление анализатора перед запуском. |
| `-d`, `--debug` | False | Режим дэбага конфигурации `setup.cfg`. Отображает результат объединения <br>локальной и глобальной конфигурации анализатора. Не запускает проверку кода. |
| `-p`, `--path` | Текущая директория | Путь до проверяемого проекта. |

Дополнительно
-------------

Поддерживается возможность настройки индивидуального CodeStyle для проверяемого проекта. Для этого в корне проекта следует создать файл конфигурации setup.cfg, пример:

```toml
[flake8]
exclude = .git,.tox,tests,package,utils
...
```

При отсутствии setup.cfg в корне проекта, для проверки кода будет взят [глобальный](styles/setup.cfg).

Использование переменных для запуска
------------------------------------

```bash
-e LINTER_ARGS=d # отображает в терминале конфигурацию, используемую анализатором при проверке кода.
```

Пример использования:
```bash
docker run -e "LINTER_ARGS=d" --rm -v "$PWD:/src" registry.gitlab.com/ivideon-public/cloud/linter-configs:latest
```
Использование linter-configs в gitlab-ci
----------------------------------------

Для запуска анализатора linter-configs в gitlab-ci потребуется выполнить include конфигурации в **.gitlab-ci.yml** следующим способом:

```yaml
include:
  - project: ivideon-public/cloud/linter-configs
    file: /gitlab-ci-linter.yml
```

Сам job:

```yaml
run-lint:
  stage: lint
  extends: .lint
  when: always
```

Окончательный пример:

```yaml
stages:
  - lint

include:
  - project: ivideon-public/cloud/linter-configs
    file: /gitlab-ci-linter.yml

run-lint:
  stage: lint
  extends: .lint
  when: always
```